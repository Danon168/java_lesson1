package ru.unn.lesson1;

import java.util.Scanner;
import java.io.IOException;

public class Main {

    static int firstValue, secondValue, result;

    public static void main(String[] args) throws IOException {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите какую операцию вы хотите выполнить(+,-,*,/): ");
        char operation;
        operation = (char) System.in.read();
         firstValue = 0;
        System.out.println("Введите первое число: ");
        int firstValue = in.nextInt();
        secondValue = 0;
        System.out.println("Введите второе число: ");
        int secondValue = in.nextInt();
        switch (operation) {
            case '+': result = firstValue + secondValue;
            System.out.println(firstValue +"+"+ secondValue +"="+ result);
            break;
            case '-': result = firstValue - secondValue;
            System.out.println(firstValue +"-"+ secondValue +"="+ result);
            break;
            case '*': result = firstValue * secondValue;
            System.out.println(firstValue +"*"+ secondValue +"="+ result);
            break;
            case '/': result = firstValue / secondValue;
            System.out.println(firstValue +"/"+ secondValue +"="+ result);
            break;
            default:
                System.out.println("Несуществующая операция!");


        }
    }
}
