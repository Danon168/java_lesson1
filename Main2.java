package ru.unn.lesson2;

import java.util.Scanner;

public class Main2 {

    static double hourWorked, payday, costs, overtime;

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите кол-во часов:");
        hourWorked = in.nextInt();
        if (hourWorked > 60) {
            System.out.println("Неверное условие! Кол-во часов не должно быть больше 60");
        } else {
            System.out.println("Введите зарплату работника:");
            payday = in.nextInt();
            if (payday < 8) {
                System.out.println("Неверное условие! Зарплата должна быть больше 8$");
            } else {

                if (hourWorked < 40) {
                    costs = hourWorked * payday;
                    System.out.println(costs);
                } else {
                    payday = payday + (payday*0.5);
                    overtime = payday * hourWorked;
                    System.out.println(overtime);
                }
            }
        }
    }
}
